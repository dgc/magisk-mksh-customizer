#!/system/bin/sh

MODDIR=${0%/*}
ETCDIR=/data/local/tmp/mksh.d

BINDS=
bind () {
	src=$1
	dst=$2
	shift 2
	[ -d "$src" ] && mkdir -p "${dst}"
	mount -obind,${*-ro} "$src" "$dst"
	BINDS="$BINDS $dst"
}

mkdir -p ${MODDIR}/mksh.d/local
chown -R shell:sdcard_rw ${MODDIR}/mksh.d/local
chmod -R ug+w ${MODDIR}/mksh.d/local
touch ${MODDIR}/mkshrc
bind ${MODDIR}/mksh.d ${ETCDIR}

# Copy native mkshrc so we can replace it and still source it
cp /etc/mkshrc ${MODDIR}/mksh.d/mkshrc.system
bind ${MODDIR}/mkshrc /etc/mkshrc

cat <<EOF  >${MODDIR}/mkshrc
# This is where customizations may persist. $MKSHETC/mkshrc will always
# be sourced, followed by $MKSHETC/[0-9][0-9]-*.
MKSHETC=${ETCDIR}/local

# Source our copy of the native mkshrc
. ${ETCDIR}/mkshrc.system

# Source the module's mkshrc files
. ${ETCDIR}/mkshrc.module
. ${ETCDIR}/mkshrc.busybox

# Enable busybox aliases. \`bbdisable\` to disable them.
bbenable

# Source local files
[ -f ${MKSHETC}/mkshrc ] && . ${MKSHETC}/mkshrc
for file in ${MKSHETC}/[0-9][0-9]-*; do
	[ -f "${file}" ] && . "${file}"
done

# Finally let /sdcard/.mkshrc be sourced, too.
[ -f /sdcard/.mkshrc ] && . /sdcard/.mkshrc

mksh_umount () {
	for bind in ${BINDS}; do
		umount "\$bind"
	done
}
EOF

# Make busybox alias file
bb () { /data/adb/magisk/busybox "$@"; }
(
	echo 'bb () { /data/adb/magisk/busybox "$@"; }'
	echo

	cmdlist=$(
		bb --list |
		grep -v '\[' |
		while read cmd; do
			[ -f /system/bin/$cmd ] || echo $cmd
		done
	)

	echo "bbenable () {"
	for cmd in $cmdlist; do
		echo "	alias $cmd='bb $cmd'"
	done
	echo "}"

	echo "bbdisable () {"
	for cmd in $cmdlist; do
		echo "	unalias $cmd"
	done
	echo "}"
) >${MODDIR}/mksh.d/mkshrc.busybox
