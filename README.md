# MKSH Customizer (with busybox)

`mksh` is the basic Android shell. It has no configuration files that
are user-modifiable and persistent, but I use shell a lot and I want to
tweak that environment. This module gives me tools to do so. It also
enables busybox commands, using the busybox distributed with Magisk.

Upon startup, MKSH Customizer copies the system `/etc/mkshrc` file to
its own module storage, then bind-mounts a customizing framework over
it. The customizing framework is generated at boot as well. It reads
additional shell configuration from several sources:

* `<module>/mksh.d/mkshrc.module`

  This is distributed with the module and lets me give you nice toys.

* `<module>/mksh.d/mkshrc.busybox`

  This is generated when the device starts up. For each command that
  Magisk's busybox supports but that doesn't exist on your device in
  `/system/bin`, it creates a shell alias to run that command via
  busybox. No wacky directories full of symlinks added to your path —
  just aliases using tools you already have.

  The `bbdisable` will deactivate all busybox aliases. `bbenable` will
  reënable them.

* `$MKSHETC/mkshrc`

  `$MKSHETC` is a place, and it's persistent. Don't worry about what it
  is, just call it `$MKSHETC`. You can put your own configuration here.

* `$MKSHETC/[0-9][0-9]-*`

  You can also add additional shell code to files in this folder, and
  they will be sourced in order.


# Building the Magisk module zipfile

Just use `make`. You need `python3` available.

	git clone https://gitlab.com/dgc/magisk-embargo
	cd magisk-embargo
	make
	adb push embargo.zip /sdcard/Download


# History

## 0.1 - 20211204

